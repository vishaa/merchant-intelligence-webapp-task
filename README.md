# Merchant Intelligence Webapp
Proceed the following instructions to run the app locally.

## Install Python3
Install python3 using the python installer from the official site
[Official Release ](https://www.python.org/downloads/release)

## Install Google cloud SDK
[Install gcloud sdk](https://cloud.google.com/sdk/docs)
Once you have google cloud sdk installed, run the following command.
```
gcloud components install cloud-datastore-emulator
```
Set the google appengine credentials by running the following commander.
```
export GOOGLE_APPLICATION_CREDENTIALS="path-to-gky.json"
```

## Run Datastore Emulator
Run the following command.
```
gcloud beta emulators datastore start
```

## Setup Virtual Environment
Open new terminal window and in the project root directory, run the following command.
```
python3 -m venv venv
```

## Activate Virtual Environment
Run the following command.
```
source venv/bin/activate
```

## Install dependencies
Run the following command.
```
pip3 install -r requirements.txt
```

## Run the  App
Run the following command.
```
python3 main.py
```
