import json
import logging

from flask import Blueprint, request, render_template

from services.helpers import ComplexEncoder
from services.merchant_service import MerchantService

MERCHANT_VIEWS = Blueprint(
    'merchant_views',
    __name__,
)


@MERCHANT_VIEWS.route('/')
def index():
    return render_template("index.html")


@MERCHANT_VIEWS.route('/merchants/view/<merchant_id>')
def merchant_view(merchant_id):
    return render_template('merchant-view.html', merchant_id=merchant_id)


@MERCHANT_VIEWS.route('/merchants/add', methods=["POST"])
def add_merchant():
    json_body = request.get_json(force=True)
    url = json_body.get("url")
    name = json_body.get("name")
    success, message = MerchantService.add_merchant(url=url, name=name)
    return json.dumps({
        "success": success,
        "message": message
    })


@MERCHANT_VIEWS.route('/merchants')
def get_merchants():
    success, merchants_list = MerchantService.get_merchants()
    response = {
        "success": success,
        "merchants": merchants_list
    }
    json_response = json.dumps(response, cls=ComplexEncoder)
    return json_response


@MERCHANT_VIEWS.route('/merchants/<merchant_id>')
def get_merchant_data(merchant_id):
    source = request.args.get("source", "all")
    logging.info(merchant_id)
    merchant_data_dict = MerchantService.get_merchant_data(merchant_id, source)
    response = {
        "success": True,
        "data": merchant_data_dict
    }
    json_response = json.dumps(response, cls=ComplexEncoder)
    return json_response

