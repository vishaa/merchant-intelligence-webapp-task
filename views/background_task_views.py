import logging
from traceback import format_exc

from flask import Blueprint

from services.merchant_service import MerchantService

BACKGROUND_TASK_VIEWS = Blueprint(
    'background_task_views',
    __name__,
)


@BACKGROUND_TASK_VIEWS.route('/internal/merchants/sync')
def sync_merchants():
    success, merchants = MerchantService.get_merchants()
    for merchant in merchants:
        try:
            domain = merchant.get("id")
            logging.info("domain: %s" %domain)
            MerchantService.sync_merchant_data(domain)
        except Exception as e:
            logging.info(e)
            logging.info(format_exc())
    return "Okay"
