from flask import Blueprint
from config import ENV
from google.cloud import ndb
from models.built_with_data import BuiltWithData
from models.merchant import Merchant
from models.similar_web_data import SimilarWebData
from models.uplead_data import UpLeadData
from models.awis_data import AWISData


INTERNAL_VIEWS = Blueprint(
    'internal_views',
    __name__,
)


# @INTERNAL_VIEWS.route('/flush-datastore/xxcd')
# def flush_datastore():
#     if ENV == "local":
#         ndb.delete_multi(
#             Merchant.query().fetch(keys_only=True)
#         )
#         ndb.delete_multi(
#             BuiltWithData.query().fetch(keys_only=True)
#         )
#         ndb.delete_multi(
#             SimilarWebData.query().fetch(keys_only=True)
#         )
#         ndb.delete_multi(
#             UpLeadData.query().fetch(keys_only=True)
#         )
#         ndb.delete_multi(
#             AWISData.query().fetch(keys_only=True)
#         )
#         return "Flushed!!"
#     return "Not Flushed!!"