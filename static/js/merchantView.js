document.addEventListener('DOMContentLoaded', function() {

     var merchantView = document.getElementById("merchant-view");
     var tableBodyElm = document.getElementById("table-body");

    function loadUpleadData(upleadData) {
        try {
          let data = upleadData.data.data;
          let tableData = {
            Domain: data.domain,
            CompanyName: data.company_name,
            Description: data.description,
            Country: data.country,
            Revenue: data.revenue,
            YearFounded: data.year_founded,
          }

          Object.entries(tableData).forEach(
            ([key, value]) => {tableBodyElm.innerHTML += `
            <tr>
              <th scope="row">${key}</th>
              <td colspan="2" class="table-active">${value}</td>
            </tr>
          `;
          }
          );
        } catch(err) {
         console.log(err);
        }
    }

    function loadAwisData(awisData) {
        try {
          let usageData = awisData.data.Awis.Results.Result.Alexa.TrafficData.UsageStatistics.UsageStatistic;
          console.log(usageData);

          usageData.map(data => {
            tableBodyElm.innerHTML += `
            <tr>
              <th scope="row">TrafficData</th>
              <td colspan="2" class="table-active">"<pre>"+${JSON.stringify(data, null, 2)}+"</pre>"</td>
            </tr>
            `;
          });
        } catch(err) {
         console.log(err);
        }
    }

    function loadBuiltWithData(builtWithData) {
        try {
          let data = builtWithData.data.Results[0];
          let meta = data.Meta;
          console.log(meta);

          let tableData = {
            Emails: "<pre>"+JSON.stringify(meta.Emails, null, 2)+"</pre>",
            Social: "<pre>"+JSON.stringify(meta.Social, null, 2)+"</pre>",
            Telephones: "<pre>"+JSON.stringify(meta.Telephones, null, 2)+"</pre>"
          }

          Object.entries(tableData).forEach(
            ([key, value]) => {tableBodyElm.innerHTML += `
            <tr>
              <th scope="row">${key}</th>
              <td colspan="2" class="table-active">${value}</td>
            </tr>
          `;
          }
          );

        } catch(err) {
         console.log(err);
        }
    }


    function loadOpenGraphData(openGraphData) {
        try {
          let data = openGraphData.data;
          console.log(data);

          Object.entries(data).forEach(
            ([key, value]) => {tableBodyElm.innerHTML += `
            <tr>
              <th scope="row">OpenGraph Data</th>
              <td colspan="2" class="table-active"><b>${key}</b> : ${value}</td>
            </tr>
          `;
          }
          );

        } catch(err) {
         console.log(err);
        }
    }


    function loadMerchantView(data) {
        var builtWithData = data.built_with_data;
        var awisData = data.awis_data;
        var similarWebData = data.similar_web_data;
        var upleadData = data.uplead_data;
        var openGraphData = data.opengraph_data;

        loadUpleadData(upleadData);
        loadBuiltWithData(builtWithData);
        loadOpenGraphData(openGraphData);
        loadAwisData(awisData);
    }

    function getMerchantDetails() {
        var merchantId = document.getElementById("merchant-id").value;
        var xmlHttp = new XMLHttpRequest();
        let url = '/merchants/' + merchantId;
        xmlHttp.open('GET', url, true);
        xmlHttp.send();
        xmlHttp.onreadystatechange = function() {
            if(this.readyState == 4 && this.status == 200) {
                let obj = JSON.parse(this.responseText);
                let success = obj.success;
                let data = obj.data;
                console.log(obj);
                if(success == true){
                    loadMerchantView(data);
                }
                else {
                    console.log("Error getting merchant Detail");
                    alert("Error Loading Data");
                }
            }
        };
    }

    getMerchantDetails();

});