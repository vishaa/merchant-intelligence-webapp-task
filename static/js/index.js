document.addEventListener('DOMContentLoaded', function() {

    document.getElementById("add-merchant").addEventListener("click", function() {
        var addMerchantInputElm = document.getElementById("add-merchant-input");
        let url = addMerchantInputElm.value;
        addMerchant(url);
    });

    function addMerchant(url) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open('POST','/merchants/add',true);
        xmlHttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xmlHttp.send(JSON.stringify({
         "url": url
         }));
        xmlHttp.onreadystatechange = function() {
            if(this.readyState == 4 && this.status == 200) {
                let obj = JSON.parse(this.responseText);
                alert(obj.message);
                getMerchants();
            }
        };
    }

    function loadMerchants(merchants) {
        var merchantView = document.getElementById("merchant-view");
        merchantView.innerHTML = "";
        if(merchants.length === 0) {
            merchantView.innerHTML += `
                <a class="list-group-item list-group-item-action">No Merchants Found</a>
            `;
            return;
        }

        merchants.map( merchant => {
            let merchantId = merchant.id;
            merchantHref = "/merchants/view/" + merchantId
            merchantView.innerHTML
            merchantView.innerHTML += `
                <a href=${merchantHref} class="list-group-item list-group-item-action">${merchant.name}</a>
            `;
        }
        );
    }

    function getMerchants() {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open('GET','/merchants',true);
        xmlHttp.send();
        xmlHttp.onreadystatechange = function() {
            if(this.readyState == 4 && this.status == 200) {
                let obj = JSON.parse(this.responseText);
                let success = obj.success;
                if(success == true){
                    let merchantList = obj.merchants;
                    loadMerchants(merchantList);
                }
                else {
                    console.log("Error getting merchants");
                    let merchantView = document.getElementById("merchant-view");
                    merchantView.innerHTML += `
                        <a class="list-group-item list-group-item-action">Error Loading Merchants</a>
                    `;
                }
            }
        };
    }

    getMerchants();

});