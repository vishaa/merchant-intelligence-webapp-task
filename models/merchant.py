from google.cloud import ndb

class Merchant(ndb.Model):
    name = ndb.StringProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)
    updated = ndb.DateTimeProperty(auto_now=True)

    @staticmethod
    def add_merchant(domain, name):
        merchant = Merchant(
            id=domain,
            name=name,
        )
        merchant.put()
        return merchant

    @staticmethod
    def get_merchant(domain):
        return Merchant.get_by_id(domain)


    @staticmethod
    def get_all_merchants():
        merchants = Merchant.query().fetch()
        return merchants
