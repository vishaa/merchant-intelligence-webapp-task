from google.cloud import ndb

class UpLeadData(ndb.Model):
    data = ndb.JsonProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)
    updated = ndb.DateTimeProperty(auto_now=True)

    @staticmethod
    def add_data(domain, data):
        UpLeadData(
            id=domain,
            data=data,
        ).put()

    @staticmethod
    def get_data_by_id(merchant_id):
        return UpLeadData.get_by_id(merchant_id)
