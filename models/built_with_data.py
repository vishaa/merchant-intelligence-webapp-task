from google.cloud import ndb

class BuiltWithData(ndb.Model):
    data = ndb.JsonProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)
    updated = ndb.DateTimeProperty(auto_now=True)

    @staticmethod
    def add_data(domain, data):
        BuiltWithData(
            id=domain,
            data=data,
        ).put()

    @staticmethod
    def get_data_by_id(merchant_id):
        return BuiltWithData.get_by_id(merchant_id)
