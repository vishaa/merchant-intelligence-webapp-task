import warnings
from google.cloud import ndb


with warnings.catch_warnings():
    warnings.filterwarnings("ignore",
                            message="UserWarning: Your application has authenticated using end user credentials \
                            from Google Cloud SDK. *",
                            category=UserWarning,
                            module='UserWarning: Your application has authenticated using end user credentials \
                            from Google Cloud SDK. *')
    client = ndb.Client()

class NDBMiddleware:
    def __init__(self, app):
        self.app = app
        self.client = client

    def __call__(self, environ, start_response):
        with self.client.context():
            return self.app(environ, start_response)
