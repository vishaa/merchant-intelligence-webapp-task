import json
import logging
from traceback import format_exc

import requests

from models.similar_web_data import SimilarWebData

URL = "https://data.similarweb.com/api/v1/data?domain=%s"

class SimilarWebDataService(object):

    @staticmethod
    def sync_data(domain):
        success, data = SimilarWebDataService.get_similar_web_data(domain)
        if success:
            SimilarWebData.add_data(
                domain=domain,
                data=data,
            )

    @staticmethod
    def get_similar_web_data(domain):
        try:
            url = URL % domain
            response = requests.request(
                url=url,
                method="GET",
                timeout=20,
            )
            logging.info("SimilarWebData Res: %s" % response.content)

            if response.status_code == 200:
                response_dict = json.loads(response.content)
                return True, response_dict
            else:
                return False, {}
        except Exception as e:
            logging.info(e)
            logging.info(format_exc())
            return False, {}
