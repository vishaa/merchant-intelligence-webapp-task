import json
import logging
from traceback import format_exc

import requests

from config import API_KEYS
from models.built_with_data import BuiltWithData

URL = "https://api.builtwith.com/v19/api.json?KEY=%s&LOOKUP=%s"
BUILT_WITH_API_KEY = API_KEYS.get("built_with")

class BuiltWithAPIService(object):

    @staticmethod
    def sync_data(domain):
        success, data = BuiltWithAPIService.get_built_with_data(domain)
        if success:
            BuiltWithData.add_data(
                domain=domain,
                data=data
            )

    @staticmethod
    def get_built_with_data(domain):
        try:
            url = URL % (BUILT_WITH_API_KEY, domain)
            response = requests.request(
                url=url,
                method="GET",
                timeout=20,
            )
            logging.info("BuiltWith Res: %s" % response.content)

            if response.status_code == 200:
                response_dict = json.loads(response.content)
                return True, response_dict
            else:
                return False, {}
        except Exception as e:
            logging.info(e)
            logging.info(format_exc())
            return False, {}
