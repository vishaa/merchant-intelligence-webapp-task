import logging
from traceback import format_exc

from models.awis_data import AWISData
from models.built_with_data import BuiltWithData
from models.merchant import Merchant
from models.similar_web_data import SimilarWebData
from models.uplead_data import UpLeadData
from models.opengraph_data import OpenGraphData
from services.awis_data_service import AWISDataService
from services.built_with_api_service import BuiltWithAPIService
from services.helpers import get_domain_from_url
from services.similar_web_data_service import SimilarWebDataService
from services.uplead_data_service import UpLeadDataService
from services.opengraph_service import OpenGraphDataService


class MerchantService(object):

    @staticmethod
    def add_merchant(url, name):
        try:
            domain = get_domain_from_url(url)
            if not domain:
                return False, "Invalid url."

            merchant = Merchant.get_merchant(domain)
            name = name if name else domain
            if merchant:
                return False, "Domain already added."

            merchant_obj = Merchant.add_merchant(domain, name)
            MerchantService.sync_merchant_data(domain, merchant_obj)
            return True, "Added domain successfully."
        except Exception as e:
            logging.info(e)
            logging.info(format_exc())
            return False, "Error adding merchant"

    @staticmethod
    def get_merchants():
        merchants_list = []
        try:
            merchants = Merchant.get_all_merchants()
            for merchant in merchants:
                merchant_dict = merchant.to_dict()
                merchant_dict["id"] = merchant.key.id()
                merchants_list.append(merchant_dict)
            return True, merchants_list
        except Exception as e:
            logging.info(e)
            logging.info(format_exc())
            return False, []

    @staticmethod
    def sync_merchant_data(domain, merchant_obj=None):
        uplead_data = UpLeadDataService.sync_data(domain)
        logging.info(uplead_data)

        if uplead_data and uplead_data.get("data") and merchant_obj:
            company_name = uplead_data.get("data", {}).get("company_name")
            logging.info(company_name)
            if company_name:
                merchant_obj.name = company_name
                merchant_obj.put()

        BuiltWithAPIService.sync_data(domain)
        SimilarWebDataService.sync_data(domain)
        AWISDataService.sync_data(domain)
        OpenGraphDataService.sync_data(domain)


    @staticmethod
    def get_merchant_data(merchant_id, source="all"):
        data = {}

        if source in ("all", "built-with"):
            built_with_data = MerchantService.get_built_with_data(merchant_id)
            data.update(built_with_data=built_with_data)

        if source in ("all", "similar-web"):
            similar_web_data = MerchantService.get_similar_web_data(merchant_id)
            data.update(similar_web_data=similar_web_data)

        if source in ("all", "uplead"):
            uplead_data = MerchantService.get_uplead_data(merchant_id)
            data.update(uplead_data=uplead_data)

        if source in ("all", "awis"):
            awis_data = MerchantService.get_awis_data(merchant_id)
            data.update(awis_data=awis_data)

        if source in ("all", "opengraph"):
            opengraph_data = MerchantService.get_opengraph_data(merchant_id)
            data.update(opengraph_data=opengraph_data)

        return data

    @staticmethod
    def get_built_with_data(merchant_id):
        try:
            merchant_bw_data = BuiltWithData.get_data_by_id(merchant_id)
            merchant_data_dict = merchant_bw_data.to_dict()
            merchant_data_dict["id"] = merchant_bw_data.key.id()
            return merchant_data_dict
        except Exception as e:
            logging.info(e)
            logging.info(format_exc())
            return {}

    @staticmethod
    def get_similar_web_data(merchant_id):
        try:
            merchant_sm_data = SimilarWebData.get_data_by_id(merchant_id)
            merchant_data_dict = merchant_sm_data.to_dict()
            merchant_data_dict["id"] = merchant_sm_data.key.id()
            return merchant_data_dict
        except Exception as e:
            logging.info(e)
            logging.info(format_exc())
            return {}

    @staticmethod
    def get_uplead_data(merchant_id):
        try:
            merchant_uplead_data = UpLeadData.get_data_by_id(merchant_id)
            merchant_data_dict = merchant_uplead_data.to_dict()
            merchant_data_dict["id"] = merchant_uplead_data.key.id()
            return merchant_data_dict
        except Exception as e:
            logging.info(e)
            logging.info(format_exc())
            return {}

    @staticmethod
    def get_awis_data(merchant_id):
        try:
            merchant_awis_data = AWISData.get_data_by_id(merchant_id)
            merchant_data_dict = merchant_awis_data.to_dict()
            merchant_data_dict["id"] = merchant_awis_data.key.id()
            return merchant_data_dict
        except Exception as e:
            logging.info(e)
            logging.info(format_exc())
            return {}

    @staticmethod
    def get_opengraph_data(merchant_id):
        try:
            merchant_opg_data = OpenGraphData.get_data_by_id(merchant_id)
            merchant_data_dict = merchant_opg_data.to_dict()
            merchant_data_dict["id"] = merchant_opg_data.key.id()
            return merchant_data_dict
        except Exception as e:
            logging.info(e)
            logging.info(format_exc())
            return {}
