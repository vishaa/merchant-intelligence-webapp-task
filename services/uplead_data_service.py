import json
import logging
from traceback import format_exc

import requests

from models.uplead_data import UpLeadData
from config import API_KEYS

URL = "https://api.uplead.com/v2/company-search?domain=%s"
UPLEAD_API_KEY = API_KEYS.get("uplead")

class UpLeadDataService(object):

    @staticmethod
    def sync_data(domain):
        success, data = UpLeadDataService.get_uplead_data(domain)
        if success:
            UpLeadData.add_data(
                domain=domain,
                data=data,
            )
            return data
        return {}

    @staticmethod
    def get_uplead_data(domain):
        try:
            url = URL % domain
            response = requests.request(
                url=url,
                method="GET",
                headers={
                    "Authorization": UPLEAD_API_KEY,
                },
                timeout=20,
            )
            logging.info("UpleadData Res: %s" % response.content)

            if response.status_code == 200:
                response_dict = json.loads(response.content)
                return True, response_dict
            else:
                return False, {}
        except Exception as e:
            logging.info(e)
            logging.info(format_exc())
            return False, {}
