import json
import logging
from traceback import format_exc

import requests

from config import API_KEYS
from models.awis_data import AWISData

URL = "https://awis.api.alexa.com/api?Action=urlInfo&ResponseGroup=UsageStats&Output=json&Url=%s"
AWIS_API_KEY = API_KEYS.get("awis")

class AWISDataService(object):

    @staticmethod
    def sync_data(domain):
        success, data = AWISDataService.get_awis_data(domain)
        if success:
            AWISData.add_data(
                domain=domain,
                data=data,
            )

    @staticmethod
    def get_awis_data(domain):
        try:
            url = URL % domain
            response = requests.request(
                url=url,
                method="GET",
                headers={
                    "x-api-key": AWIS_API_KEY,
                },
                timeout=20,
            )
            logging.info("AWISData Res: %s" % response.content)

            if response.status_code == 200:
                response_dict = json.loads(response.content)
                return True, response_dict
            else:
                return False, {}
        except Exception as e:
            logging.info(e)
            logging.info(format_exc())
            return False, {}
