import logging
from traceback import format_exc

import opengraph_py3

from models.opengraph_data import OpenGraphData


class OpenGraphDataService(object):

    @staticmethod
    def sync_data(domain):
        success, data = OpenGraphDataService.get_opengraph_data(domain)
        if success:
            OpenGraphData.add_data(
                domain=domain,
                data=data,
            )

    @staticmethod
    def get_opengraph_data(domain):
        try:
            opengraph_dict = {}
            url = "https://www.%s" % domain
            data = opengraph_py3.OpenGraph(url=url)
            if data.is_valid():
                logging.info("opengraph data: %s" % data.items())
                for key, value in data.items():
                    opengraph_dict[key] = value
                return True, opengraph_dict
            else:
                return False, {}
        except Exception as e:
            logging.info(e)
            logging.info(format_exc())
            return False, {}
