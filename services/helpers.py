import datetime
import json
import re
from urllib.parse import urlparse

from google.cloud.ndb import Model
from google.cloud.ndb.key import Key


class ComplexEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return int((obj - datetime.datetime(1970, 1, 1)).total_seconds() * 1000)
        if isinstance(obj, Key):
            return obj.urlsafe()
        if isinstance(obj, Model):
            return obj.to_dict()
        return None

def get_domain_from_url(url):
    host_name = urlparse(url).netloc
    domain = re.sub(r'www.', '', host_name)
    return domain
