import logging
import os

from flask import Flask

from models import NDBMiddleware
from views.background_task_views import BACKGROUND_TASK_VIEWS
from views.internal_views import INTERNAL_VIEWS
from views.merchant_views import MERCHANT_VIEWS

logging.basicConfig(
    format='%(levelname)-8s %(asctime)s,%(msecs)d  [%(filename)s:%(lineno)d] %(message)s',
    datefmt='%Y-%m-%d:%H:%M:%S',
    level=logging.INFO
)

root_path = os.path.abspath('.')
static_path = os.path.join(root_path, 'static')
template_dir = os.path.join(static_path, 'templates')

app = Flask(__name__, template_folder=template_dir)

app.wsgi_app = NDBMiddleware(app.wsgi_app)
app.register_blueprint(MERCHANT_VIEWS)
app.register_blueprint(INTERNAL_VIEWS)
app.register_blueprint(BACKGROUND_TASK_VIEWS)


if __name__ == '__main__':
    app.run(debug=True)
